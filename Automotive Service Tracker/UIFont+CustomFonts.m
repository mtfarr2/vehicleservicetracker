//
//  UIFont+CustomFonts.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/7/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "UIFont+CustomFonts.h"

@implementation UIFont (CustomFonts)

+(UIFont*)textFieldFont{
    return [UIFont fontWithName:@"Marker Felt" size:16];
}

+(UIFont*)textFont{
    return [UIFont fontWithName:@"Marker Felt" size:14];
}

@end
