//
//  MaintenanceHistoryViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import "AddMaintenanceViewController.h"

@interface MaintenanceHistoryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>
{
    UITableView* myTableView;
    AppDelegate* ad;
    NSArray* carsMaintenance;
    NSDateFormatter* formatter;
    NSString* dateString;
    UIView* descriptionView;
    NSString* messageBody;
    //MFMailComposeViewController* controller;
}
@end
