//
//  AppDelegate.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 3/30/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import "Car.h"
#import <Parse/Parse.h>
#import <MessageUI/MessageUI.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Car* curCar;
@property (strong, nonatomic) NSMutableArray* carObjects;
@property (strong, nonatomic) PFObject* curPFCar;
@property (strong, nonatomic) MFMailComposeViewController* globalMailComposer;
@property (strong, nonatomic)NSArray* carParts;
-(void)cycleTheGlobalMailComposer;
@end

