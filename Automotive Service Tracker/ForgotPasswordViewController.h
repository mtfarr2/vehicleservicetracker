//
//  ForgotPasswordViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/21/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import <Parse/Parse.h>

@interface ForgotPasswordViewController : UIViewController
{
    UITextField* email;
    UIButton* submitBtn;
}
@end
