//
//  RootTabbedViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "RootTabbedViewController.h"

@interface RootTabbedViewController ()

@end

@implementation RootTabbedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UpdateMileageViewController* umvc = [[UpdateMileageViewController alloc]init];
    UpcomingMaintenanceViewController* ucmvc = [[UpcomingMaintenanceViewController alloc]init];
    MaintenanceHistoryViewController* mhvc = [[MaintenanceHistoryViewController alloc]init];
    TabbedViewController* tvc = [[TabbedViewController alloc]init];
    
    
    NSArray* vcArray = @[ucmvc, umvc, mhvc];
    [tvc setViewControllers:vcArray];
    
    [self.view addSubview:tvc.view];
    [self addChildViewController:tvc];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
