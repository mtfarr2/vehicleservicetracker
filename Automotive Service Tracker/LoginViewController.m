//
//  LoginViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 3/30/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Login";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    UITapGestureRecognizer* tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelKeyboard:)];
    [self.view addGestureRecognizer:tgr];
    
    userName = [[UITextField alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 50)];
    userName.backgroundColor = [UIColor headerColor];
    UIColor *color = [UIColor lightTextColor];
    userName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Username" attributes:@{NSForegroundColorAttributeName: color}];
    userName.textColor = [UIColor lightGrayColor];
    [self.view addSubview:userName];
    
    password = [[UITextField alloc]initWithFrame:CGRectMake(0, userName.frame.origin.y + 50, self.view.frame.size.width, 50)];
    password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Password" attributes:@{NSForegroundColorAttributeName: color}];
    password.textColor = [UIColor lightGrayColor];
    password.backgroundColor = [UIColor headerColor];
    password.secureTextEntry = YES;
    [self.view addSubview:password];
    
    UIButton* loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(0, password.frame.origin.y + password.frame.size.height, self.view.frame.size.width, 60);
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"RedBtn"] forState:UIControlStateNormal];
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"RedBtnTouched"] forState:UIControlStateHighlighted];
    [loginBtn setTitle:@"Login" forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(loginBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    
    UILabel* loginSeperator = [[UILabel alloc]initWithFrame:CGRectMake(0, loginBtn.frame.origin.y + loginBtn.frame.size.height, self.view.frame.size.width, 50)];
    loginSeperator.text = [NSString stringWithFormat:@"--or--"];
    loginSeperator.textAlignment = NSTextAlignmentCenter;
    loginSeperator.textColor = [UIColor lightGrayColor];
    [self.view addSubview:loginSeperator];
    
    UIButton* signUpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    signUpBtn.frame = CGRectMake(0, loginSeperator.frame.origin.y + loginSeperator.frame.size.height, self.view.frame.size.width, 60);
    [signUpBtn setBackgroundImage:[UIImage imageNamed:@"YellowBtn"] forState:UIControlStateNormal];
    [signUpBtn setBackgroundImage:[UIImage imageNamed:@"YellowBtnTouched"] forState:UIControlStateHighlighted];
    signUpBtn.backgroundColor = [UIColor redColor];
    [signUpBtn setTitle:@"Create Account" forState:UIControlStateNormal];
    [signUpBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [signUpBtn addTarget:self action:@selector(signUpBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signUpBtn];
    
    UIButton* forgotPasswordBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    forgotPasswordBtn.frame = CGRectMake(self.view.frame.size.width/4, signUpBtn.frame.origin.y + signUpBtn.frame.size.height + 20, self.view.frame.size.width/2, 50);
    [forgotPasswordBtn setTitle:@"Forgot password?" forState:UIControlStateNormal];
    [forgotPasswordBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [forgotPasswordBtn addTarget:self action:@selector(forgotPasswordBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgotPasswordBtn];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [userName resignFirstResponder];
    [password resignFirstResponder];
    return NO;
}

-(void)cancelKeyboard:(id)sender{
    [userName resignFirstResponder];
    [password resignFirstResponder];
}

-(void)loginBtnTouched:(id)sender{
    
    [PFUser logInWithUsernameInBackground:userName.text password:password.text block:^(PFUser* user, NSError* error){
        if(!error){
            VehicleListViewController* vlvc = [VehicleListViewController new];
            vlvc.user = user;
            [self.navigationController pushViewController:vlvc animated:YES];
        }
    }];
}

-(void)signUpBtnTouched:(id)sender{
    SignUpViewController* suvc = [SignUpViewController new];
    [self.navigationController pushViewController:suvc animated:YES];
}

-(void)forgotPasswordBtnTouched:(id)sender{
    ForgotPasswordViewController* fpvc = [ForgotPasswordViewController new];
    [self.navigationController pushViewController:fpvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
