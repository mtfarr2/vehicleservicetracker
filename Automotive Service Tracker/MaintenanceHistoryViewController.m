//
//  MaintenanceHistoryViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "MaintenanceHistoryViewController.h"

@interface MaintenanceHistoryViewController ()

@end

@implementation MaintenanceHistoryViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self){
        self.title = @"History";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Maintenance History";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
//    UITapGestureRecognizer* tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeBtnTouched:)];
//    [self.view addGestureRecognizer:tgr];
    
    myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height - 50) style:UITableViewStyleGrouped];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.backgroundColor = [UIColor clearColor];
    myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:myTableView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    UIBarButtonItem* sendEmail = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(sendEmailTouched:)];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addCustomMaintenanceTouched:)];
    
    NSArray *actionBtns = @[sendEmail, addButton];
    
    
    UINavigationController *cont = (UINavigationController*) self.parentViewController.parentViewController;
    
     cont.navigationItem.rightBarButtonItems = actionBtns;

    
    
    PFQuery* query = [PFQuery queryWithClassName:@"Maintenance"];
    [query whereKey:@"car" equalTo:ad.curPFCar];
    [query orderByDescending:@"date"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        carsMaintenance = objects;
        if(!error){
//            if (usersCars.count > ad.carObjects.count) {
//                for (long i = ad.carObjects.count; i < usersCars.count; i++) {
//                    PFObject* dictionary = [usersCars objectAtIndex:i];
//                    
//                    Car* car = [[Car new]initWithObject:dictionary];
//                    
//                    [ad.carObjects addObject:car];
//                }
//            }
            [self loadData];
        }
        
    }];
    
    
}

-(void)sendEmailTouched:(id)sender
{
//    ad.globalMailComposer = [[MFMailComposeViewController alloc]init];

    
    for (int i = 0; i < carsMaintenance.count; i++) {
        PFObject* dictionary = [carsMaintenance objectAtIndex:i];
        
        NSDate* date = [dictionary objectForKey:@"date"];
        formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"MM-dd-yyyy"];
        dateString = [formatter stringFromDate:date];
        
        NSString *mileageString = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"mileage"]];
        NSString *appendingString = [NSString stringWithFormat:@"%@\n%@\n%@\n\n%@",dateString, [dictionary objectForKey:@"partName"], mileageString, [dictionary objectForKey:@"description"]];
        
        messageBody = [messageBody stringByAppendingString:appendingString];
        //ad.globalMailComposer.mailComposeDelegate = self;
    }
    
    
    [ad.globalMailComposer setMessageBody:messageBody isHTML:NO];
    if (ad.globalMailComposer) [self presentViewController:ad.globalMailComposer animated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:^{
        [ad cycleTheGlobalMailComposer];
    }];
}

-(void)addCustomMaintenanceTouched:(id)sender{
    AddMaintenanceViewController* amvc = [AddMaintenanceViewController new];
    amvc.carParts = ad.carParts;
    [self.navigationController pushViewController:amvc animated:YES];
}

-(void)loadData{
    [myTableView reloadData];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 60;
//}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return carsMaintenance.count;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"tableViewCell"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"tableViewCellTouched"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    PFObject* dictionary = [carsMaintenance objectAtIndex:indexPath.row];
    
    UILabel* dateLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, self.view.frame.size.width/4, 40)];
    NSDate* date = [dictionary objectForKey:@"date"];
    formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    dateString = [formatter stringFromDate:date];
    
    dateLbl.text = [NSString stringWithFormat:@" %@",dateString];
    dateLbl.textColor = [UIColor lightGrayColor];
    dateLbl.textAlignment = NSTextAlignmentCenter;
    dateLbl.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
//    cell.backgroundColor = [UIColor headerColor];
    
    UILabel* partNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 3, 5, self.view.frame.size.width / 2, 40)];
    partNameLbl.text = [NSString stringWithFormat:@" %@", [dictionary objectForKey:@"partName"]];
    partNameLbl.textColor = [UIColor lightGrayColor];
    partNameLbl.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
    [cell addSubview:partNameLbl];
    [cell addSubview:dateLbl];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    PFObject* dictionary = [carsMaintenance objectAtIndex:indexPath.row];
    
    if (descriptionView) {
        [descriptionView removeFromSuperview];
    }
    
    descriptionView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4, self.view.frame.size.height / 3, 0, 0)];
    descriptionView.backgroundColor = [UIColor lightGrayColor];
    descriptionView.layer.cornerRadius = 5;
    [self.view addSubview:descriptionView];
    
    UITextView* descriptionText = [[UITextView alloc]initWithFrame:CGRectMake(5, 5, descriptionView.frame.size.width - 10, descriptionView.frame.size.height - 50)];
    descriptionText.editable = NO;
    descriptionText.textColor = [UIColor lightGrayColor];
    descriptionText.text = [dictionary objectForKey:@"description"];
    descriptionText.backgroundColor = [UIColor headerColor];
    [descriptionView addSubview:descriptionText];
    
    UIButton* closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.backgroundColor = [UIColor lightGrayColor];
    [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
    [closeBtn.titleLabel setFont:[UIFont fontWithName: @"HoeflerText-Black" size:14]];
    [closeBtn setTitleColor:[UIColor headerColor] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [descriptionView addSubview:closeBtn];
    
    [UIView animateWithDuration:.5 animations:^{
        descriptionView.frame = CGRectMake(self.view.frame.size.width/6, self.view.frame.size.height / 3, 2 *self.view.frame.size.width/3, self.view.frame.size.height/3);
        descriptionText.frame = CGRectMake(5, 5, descriptionView.frame.size.width - 10, descriptionView.frame.size.height - 50);
        closeBtn.frame = CGRectMake(descriptionView.frame.size.width/4, descriptionText.frame.origin.y + descriptionText.frame.size.height + 5, descriptionView.frame.size.width/2, 40);
    }];
}

-(void)closeBtnTouched:(id)sender{
    [descriptionView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
