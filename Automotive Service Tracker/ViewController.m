//
//  ViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 3/30/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    LoginViewController* lvc = [LoginViewController new];
    
    UINavigationController* navController = [[UINavigationController alloc]initWithRootViewController:lvc];
    [self addChildViewController:navController];
    [self.view addSubview:navController.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
