//
//  SignUpViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/1/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"SignUp";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    userName = [[UITextField alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 50)];
    userName.backgroundColor = [UIColor headerColor];
    UIColor *color = [UIColor lightTextColor];
    userName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Username" attributes:@{NSForegroundColorAttributeName: color}];
    userName.textColor = [UIColor lightGrayColor];
    [self.view addSubview:userName];
    
    password = [[UITextField alloc]initWithFrame:CGRectMake(0, userName.frame.origin.y + 50, self.view.frame.size.width, 50)];
    password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Password" attributes:@{NSForegroundColorAttributeName: color}];
    password.textColor = [UIColor lightGrayColor];
    password.backgroundColor = [UIColor headerColor];
    password.secureTextEntry = YES;
    [self.view addSubview:password];
    
    confirmPassword = [[UITextField alloc]initWithFrame:CGRectMake(0, password.frame.origin.y + 50, self.view.frame.size.width, 50)];
    confirmPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
    confirmPassword.textColor = [UIColor lightGrayColor];
    confirmPassword.backgroundColor = [UIColor headerColor];
    confirmPassword.secureTextEntry = YES;
    [self.view addSubview:confirmPassword];
    
    email = [[UITextField alloc]initWithFrame:CGRectMake(0, confirmPassword.frame.origin.y + confirmPassword.frame.size.height, self.view.frame.size.width, 50)];
    email.backgroundColor = [UIColor headerColor];
    email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Email" attributes:@{NSForegroundColorAttributeName: color}];
    email.textColor = [UIColor lightGrayColor];
    [self.view addSubview:email];
    
    confirmEmail = [[UITextField alloc]initWithFrame:CGRectMake(0, email.frame.origin.y + 50, self.view.frame.size.width, 50)];
    confirmEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Confirm Email" attributes:@{NSForegroundColorAttributeName: color}];
    confirmEmail.textColor = [UIColor lightGrayColor];
    confirmEmail.backgroundColor = [UIColor headerColor];
    [self.view addSubview:confirmEmail];
    
    UIButton* signUpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    signUpBtn.frame = CGRectMake(0, confirmEmail.frame.origin.y + confirmEmail.frame.size.height, self.view.frame.size.width, 60);
    [signUpBtn setBackgroundImage:[UIImage imageNamed:@"RedBtn"] forState:UIControlStateNormal];
    [signUpBtn setBackgroundImage:[UIImage imageNamed:@"RedBtnTouched"] forState:UIControlStateHighlighted];
    [signUpBtn setTitle:@"Sign Up" forState:UIControlStateNormal];
    [signUpBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [signUpBtn addTarget:self action:@selector(signUpBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signUpBtn];
}

-(void)signUpBtnTouched:(id)sender{
    [userName resignFirstResponder];
    [password resignFirstResponder];
    [confirmPassword resignFirstResponder];
    [email resignFirstResponder];
    [confirmEmail resignFirstResponder];
    [self checkFieldsComplete];
}

-(void)checkFieldsComplete{
    if([userName.text isEqualToString:@""] || [password.text isEqualToString:@""] || [confirmPassword.text isEqualToString:@""] || [email.text isEqualToString:@""]
       || [confirmEmail.text isEqualToString:@""]){
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You must complete all fields"delegate:nil cancelButtonTitle:@"Ok"  otherButtonTitles:nil];
        [alert show];
    }else {
        [self checkEmailandPasswordsMatch];
    }
}

-(void)checkEmailandPasswordsMatch{
    if (![password.text isEqualToString: confirmPassword.text]) {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Passwords do not match"delegate:nil cancelButtonTitle:@"Ok"  otherButtonTitles:nil];
        [alert show];
    } else if (![email.text isEqualToString: confirmEmail.text]){
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Email addresses do not match"delegate:nil cancelButtonTitle:@"Ok"  otherButtonTitles:nil];
        [alert show];
    } else {
        [self createNewUser];
        
    }
}

-(void)createNewUser{
    PFUser* newUser = [PFUser user];
    newUser.username = userName.text;
    newUser.email = email.text;
    newUser.password = password.text;
    
    [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            VehicleListViewController* vlvc = [VehicleListViewController new];
            vlvc.user = [PFUser currentUser];
            [self.navigationController pushViewController:vlvc animated:YES];
        }else{
            NSLog(@"Error");
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
