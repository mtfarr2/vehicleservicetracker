//
//  AddMaintenancePageTwoViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/22/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "AddMaintenancePageTwoViewController.h"

@interface AddMaintenancePageTwoViewController ()

@end

@implementation AddMaintenancePageTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Add Maintenance";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnTouched:)];
    self.navigationItem.rightBarButtonItem = saveBtn;
    
    textFieldView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/8, 75, self.view.frame.size.width - self.view.frame.size.width/4, 40)];
    textFieldView.backgroundColor = [UIColor headerColor];
    textFieldView.layer.cornerRadius = 5;
    [self.view addSubview:textFieldView];
    
    mileageLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 110, 40)];
    mileageLbl.textColor = [UIColor lightGrayColor];
    mileageLbl.text = @"Enter Mileage: ";
    mileageLbl.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
    [textFieldView addSubview:mileageLbl];
    
    mileage = [[UITextField alloc]initWithFrame:CGRectMake(mileageLbl.frame.origin.x + mileageLbl.frame.size.width, 0, textFieldView.frame.size.width - mileageLbl.frame.size.width, 40)];
    mileage.textColor = [UIColor lightGrayColor];
    mileage.delegate = self;
    [textFieldView addSubview:mileage];
    
    descriptionLbl = [[UILabel alloc]initWithFrame:CGRectMake(textFieldView.frame.origin.x, textFieldView.frame.origin.y + textFieldView.frame.size.height + 20, 110, 40)];
    descriptionLbl.text = @"Description: ";
    descriptionLbl.textColor = [UIColor headerColor];
    descriptionLbl.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
    [self.view addSubview:descriptionLbl];
    
    description = [[UITextView alloc]initWithFrame:CGRectMake(descriptionLbl.frame.origin.x, descriptionLbl.frame.origin.y + descriptionLbl.frame.size.height, textFieldView.frame.size.width, 200)];
    description.backgroundColor = [UIColor headerColor];
    description.textColor = [UIColor lightGrayColor];
    [self.view addSubview:description];
    
    
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [mileage resignFirstResponder];
    return NO;
}

-(void)saveBtnTouched:(id)sender
{

    if ([mileage.text isEqualToString:@""]) {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You must include the mileage"delegate:nil cancelButtonTitle:@"Ok"  otherButtonTitles:nil];
        [alert show];
    } else {
        [self addMaintenance];
    }

}

-(void)addMaintenance{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterNoStyle;
    
    NSNumber* mileageNum = [f numberFromString:mileage.text];
    
    if (mileageNum > ad.curCar.curMileage) {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Maintenance mileage must be less than or equal to current mileage of vehicle" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else {
    
    PFObject* maintenanceObj = [PFObject objectWithClassName:@"Maintenance"];
    
    for (int i = 0; i < _carParts.count; i++) {
        if ([[_carParts[i] objectForKey:@"partName"]isEqualToString:_partName]) {
            part = [_carParts objectAtIndex:i];
            standardPart = YES;
            [maintenanceObj setObject:_carParts[i] forKey:@"part"];
        }
    }
    
    
    [maintenanceObj setObject:ad.curPFCar forKey:@"car"];
    [maintenanceObj setObject:_partName forKey:@"partName"];
    [maintenanceObj setObject:mileageNum forKey:@"mileage"];
    [maintenanceObj setObject:description.text forKey:@"description"];
    [maintenanceObj setObject:_maintDate forKey:@"date"];
    
    [maintenanceObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            if (standardPart) {
                PFQuery* query = [PFQuery queryWithClassName:@"Part"];
                [query whereKey:@"objectId" equalTo:part.objectId];
                [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                    [object setObject:mileageNum forKey:@"mileageOfLast"];
                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        
                    }];
                }];
            }
            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[RootTabbedViewController class]]) {
                    [self.navigationController popToViewController:aViewController animated:NO];
                }
            }
            
        }
    }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
