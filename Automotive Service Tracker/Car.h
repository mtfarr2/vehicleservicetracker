//
//  Car.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/16/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Car : NSObject

-(instancetype)initWithObject:(PFObject*)dictionary;


@property (nonatomic, retain) NSString* carName;
@property (nonatomic, retain) NSNumber* curMileage;
@property (nonatomic, retain) NSString* carID;
@property (nonatomic, retain) NSMutableArray* carParts;
@end
