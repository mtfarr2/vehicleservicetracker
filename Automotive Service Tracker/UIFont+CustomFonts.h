//
//  UIFont+CustomFonts.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/7/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (CustomFonts)
+(UIFont*)textFieldFont;
+(UIFont*)textFont;
@end
