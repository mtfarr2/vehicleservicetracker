//
//  LoginViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 3/30/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import "ViewController.h"
#import <Parse/Parse.h>
#import "VehicleListViewController.h"
#import "SignUpViewController.h"
#import "ForgotPasswordViewController.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate>
{
    UITextField* userName;
    UITextField* password;
}
@property(strong, nonatomic)PFUser* user;
@end
