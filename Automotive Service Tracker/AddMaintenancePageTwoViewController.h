//
//  AddMaintenancePageTwoViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/22/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "RootTabbedViewController.h"

@interface AddMaintenancePageTwoViewController : UIViewController<UITextFieldDelegate>
{
    UIView* textFieldView;
    UILabel* mileageLbl;
    UITextField* mileage;
    UILabel* descriptionLbl;
    UITextView* description;
    AppDelegate* ad;
    PFObject* part;
    BOOL standardPart;
}

@property (strong, nonatomic)NSArray* carParts;
@property (strong, nonatomic)NSString* partName;
@property (strong, nonatomic)NSDate* maintDate;
@end
