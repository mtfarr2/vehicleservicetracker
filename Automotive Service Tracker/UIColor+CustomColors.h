//
//  UIColor+CustomColors.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 3/30/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+(UIColor*)backgroundColor;
+(UIColor*)headerColor;

@end
