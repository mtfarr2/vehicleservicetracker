//
//  VehicleListViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/1/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "AddNewCarViewController.h"
#import "RootTabbedViewController.h"
#import "AppDelegate.h"
#import "Car.h"

@interface VehicleListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView* myTableView;
    NSArray* usersCars;
    AppDelegate* ad;
}
@property (strong, nonatomic)PFUser* user;

@end
