//
//  AddMaintenanceViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/22/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import "AddMaintenancePageTwoViewController.h"

@interface AddMaintenanceViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
{
    UIPickerView* picker;
    NSArray* arrayOfTitles;
    UITextField* customName;
    UIView* textFieldView;
    UILabel* enterPartNameLbl;
    UIDatePicker* datePicker;
    NSString* dateString;
    NSString* partName;
    
    
}
@property (strong, nonatomic)NSArray* carParts;
@end
