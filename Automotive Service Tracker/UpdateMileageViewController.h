//
//  UpdateMileageViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"

@interface UpdateMileageViewController : UIViewController
{
    UITextField* updateMileageField;
    UIButton* submitBtn;
    AppDelegate* ad;
}

//@property(strong, nonatomic)PFUser* user;

@end
