//
//  AddNewCarViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/7/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "AddNewCarViewController.h"

@interface AddNewCarViewController ()

@end

@implementation AddNewCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Add Vehicle";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnTouched:)];
    self.navigationItem.rightBarButtonItem = saveBtn;
    
    UITapGestureRecognizer* tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelKeyboard:)];
    [self.view addGestureRecognizer:tgr];
    
    UIView* textFieldBackground = [[UIView alloc]initWithFrame:CGRectMake(20,80,self.view.frame.size.width - 40,100)];
    textFieldBackground.backgroundColor = [UIColor headerColor];
    textFieldBackground.layer.cornerRadius = 5;
    [self.view addSubview:textFieldBackground];
    
    carNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(10,0,self.view.frame.size.width/3, 40)];
    carNameLbl.text = @"Vehicle Name: ";
    carNameLbl.textColor = [UIColor lightGrayColor];
    carNameLbl.font = [UIFont textFieldFont];
    [textFieldBackground addSubview:carNameLbl];
    
    carName = [[UITextField alloc]initWithFrame:CGRectMake(carNameLbl.frame.origin.x + carNameLbl.frame.size.width, carNameLbl.frame.origin.y, self.view.frame.size.width / 2, 40)];
    carName.backgroundColor = [UIColor headerColor];
    carName.textColor = [UIColor lightGrayColor];
    carName.delegate = self;
    carName.font = [UIFont textFont];
    [textFieldBackground addSubview:carName];
    
    curMileageLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, carNameLbl.frame.origin.y + carNameLbl.frame.size.height + 10, self.view.frame.size.width/3, 40)];
    curMileageLbl.text = @"Current Mileage: ";
    curMileageLbl.textColor = [UIColor lightGrayColor];
    curMileageLbl.font = [UIFont textFieldFont];
    [textFieldBackground addSubview:curMileageLbl];
    
    curMileage = [[UITextField alloc]initWithFrame:CGRectMake(curMileageLbl.frame.origin.x + curMileageLbl.frame.size.width, curMileageLbl.frame.origin.y, self.view.frame.size.width / 2, 40)];
    curMileage.backgroundColor = [UIColor headerColor];
    curMileage.textColor = [UIColor lightGrayColor];
    curMileage.delegate = self;
    curMileage.font = [UIFont textFont];
    [textFieldBackground addSubview:curMileage];
    
    int textBoxWidth = (self.view.frame.size.width - 40)/3 - 2;
    
    mileageOfLastLbl = [[UILabel alloc]initWithFrame:CGRectMake(textBoxWidth + 22, textFieldBackground.frame.origin.y + textFieldBackground.frame.size.height + 10, textBoxWidth, 40)];
    mileageOfLastLbl.text = @"Mileage Serviced";
    mileageOfLastLbl.textColor = [UIColor lightGrayColor];
    mileageOfLastLbl.font = [UIFont textFieldFont];
    mileageOfLastLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:mileageOfLastLbl];
    
    freqLbl = [[UILabel alloc]initWithFrame:CGRectMake(mileageOfLastLbl.frame.origin.x + mileageOfLastLbl.frame.size.width + 2, textFieldBackground.frame.origin.y + textFieldBackground.frame.size.height + 10, textBoxWidth, 40)];
    freqLbl.text = @"Frequency";
    freqLbl.textColor = [UIColor lightGrayColor];
    freqLbl.font = [UIFont textFieldFont];
    freqLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:freqLbl];
    
    oilChangeLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, freqLbl.frame.origin.y + freqLbl.frame.size.height, textBoxWidth, 40)];
    oilChangeLbl.text = @"Oil Change";
    oilChangeLbl.textColor = [UIColor lightGrayColor];
    oilChangeLbl.font = [UIFont textFieldFont];
    [self.view addSubview:oilChangeLbl];
    
    tireRotationLbl = [[UILabel alloc]initWithFrame:CGRectMake(oilChangeLbl.frame.origin.x, oilChangeLbl.frame.origin.y + oilChangeLbl.frame.size.height, oilChangeLbl.frame.size.width, oilChangeLbl.frame.size.height)];
    tireRotationLbl.text = @"Tire Rotation";
    tireRotationLbl.textColor = [UIColor lightGrayColor];
    tireRotationLbl.font = [UIFont textFieldFont];
    [self.view addSubview:tireRotationLbl];
    
    brakePadsLbl = [[UILabel alloc]initWithFrame:CGRectMake(tireRotationLbl.frame.origin.x, tireRotationLbl.frame.origin.y + tireRotationLbl.frame.size.height, tireRotationLbl.frame.size.width, tireRotationLbl.frame.size.height)];
    brakePadsLbl.text = @"Brake Pads";
    brakePadsLbl.textColor = [UIColor lightGrayColor];
    brakePadsLbl.font = [UIFont textFieldFont];
    [self.view addSubview:brakePadsLbl];
    
    accessoryDriveBeltLbl = [[UILabel alloc]initWithFrame:CGRectMake(brakePadsLbl.frame.origin.x, brakePadsLbl.frame.origin.y + brakePadsLbl.frame.size.height, brakePadsLbl.frame.size.width, brakePadsLbl.frame.size.height)];
    accessoryDriveBeltLbl.text = @"Accessory Belt";
    accessoryDriveBeltLbl.textColor = [UIColor lightGrayColor];
    accessoryDriveBeltLbl.font = [UIFont textFieldFont];
    [self.view addSubview:accessoryDriveBeltLbl];
    
    coolantHosesLbl = [[UILabel alloc]initWithFrame:CGRectMake(accessoryDriveBeltLbl.frame.origin.x, accessoryDriveBeltLbl.frame.origin.y + accessoryDriveBeltLbl.frame.size.height, accessoryDriveBeltLbl.frame.size.width, accessoryDriveBeltLbl.frame.size.height)];
    coolantHosesLbl.text = @"Coolant Hoses";
    coolantHosesLbl.textColor = [UIColor lightGrayColor];
    coolantHosesLbl.font = [UIFont textFieldFont];
    [self.view addSubview:coolantHosesLbl];
    
    airFilterLbl = [[UILabel alloc]initWithFrame:CGRectMake(coolantHosesLbl.frame.origin.x, coolantHosesLbl.frame.origin.y + coolantHosesLbl.frame.size.height, coolantHosesLbl.frame.size.width, coolantHosesLbl.frame.size.height)];
    airFilterLbl.text = @"Air Filter";
    airFilterLbl.textColor = [UIColor lightGrayColor];
    airFilterLbl.font = [UIFont textFieldFont];
    [self.view addSubview:airFilterLbl];
    
    tireLbl = [[UILabel alloc]initWithFrame:CGRectMake(airFilterLbl.frame.origin.x, airFilterLbl.frame.origin.y + airFilterLbl.frame.size.height, airFilterLbl.frame.size.width, airFilterLbl.frame.size.height)];
    tireLbl.text = @"Tires";
    tireLbl.textColor = [UIColor lightGrayColor];
    tireLbl.font = [UIFont textFieldFont];
    [self.view addSubview:tireLbl];
    
    UIView* mileageOfLastView = [[UIView alloc]initWithFrame:CGRectMake(mileageOfLastLbl.frame.origin.x, mileageOfLastLbl.frame.origin.y + mileageOfLastLbl.frame.size.height, mileageOfLastLbl.frame.size.width, tireLbl.frame.origin.y - oilChangeLbl.frame.origin.y + 40)];
    mileageOfLastView.backgroundColor = [UIColor headerColor];
    mileageOfLastView.layer.cornerRadius = 5;
    [self.view addSubview:mileageOfLastView];
    
    UIView* freqView = [[UIView alloc]initWithFrame:CGRectMake(freqLbl.frame.origin.x, freqLbl.frame.origin.y + freqLbl.frame.size.height, freqLbl.frame.size.width, tireLbl.frame.origin.y - oilChangeLbl.frame.origin.y + 40)];
    freqView.backgroundColor = [UIColor headerColor];
    freqView.layer.cornerRadius = 5;
    [self.view addSubview:freqView];
    
    oilChangeMileage = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, mileageOfLastView.frame.size.width, 40)];
    oilChangeMileage.textColor = [UIColor lightGrayColor];
    oilChangeMileage.delegate = self;
    oilChangeMileage.font = [UIFont textFont];
    [mileageOfLastView addSubview:oilChangeMileage];
    
    tireRotationMileage = [[UITextField alloc]initWithFrame:CGRectMake(0, oilChangeMileage.frame.origin.y + oilChangeMileage.frame.size.height, mileageOfLastView.frame.size.width, 40)];
    tireRotationMileage.textColor = [UIColor lightGrayColor];
    tireRotationMileage.delegate = self;
    tireRotationMileage.font = [UIFont textFont];
    [mileageOfLastView addSubview:tireRotationMileage];
    
    brakePadsMileage = [[UITextField alloc]initWithFrame:CGRectMake(0, tireRotationMileage.frame.origin.y + tireRotationMileage.frame.size.height, mileageOfLastView.frame.size.width, 40)];
    brakePadsMileage.textColor = [UIColor lightGrayColor];
    brakePadsMileage.delegate = self;
    brakePadsMileage.font = [UIFont textFont];
    [mileageOfLastView addSubview:brakePadsMileage];
    
    accessoryDriveBeltMileage = [[UITextField alloc]initWithFrame:CGRectMake(0, brakePadsMileage.frame.origin.y + brakePadsMileage.frame.size.height, mileageOfLastView.frame.size.width, 40)];
    accessoryDriveBeltMileage.textColor = [UIColor lightGrayColor];
    accessoryDriveBeltMileage.delegate = self;
    accessoryDriveBeltMileage.font = [UIFont textFont];
    [mileageOfLastView addSubview:accessoryDriveBeltMileage];
    
    coolantHosesMileage = [[UITextField alloc]initWithFrame:CGRectMake(0, accessoryDriveBeltMileage.frame.origin.y + accessoryDriveBeltMileage.frame.size.height, mileageOfLastView.frame.size.width, 40)];
    coolantHosesMileage.textColor = [UIColor lightGrayColor];
    coolantHosesMileage.delegate = self;
    coolantHosesMileage.font = [UIFont textFont];
    [mileageOfLastView addSubview:coolantHosesMileage];
    
    airFilterMileage = [[UITextField alloc]initWithFrame:CGRectMake(0, coolantHosesMileage.frame.origin.y + coolantHosesMileage.frame.size.height, mileageOfLastView.frame.size.width, 40)];
    airFilterMileage.textColor = [UIColor lightGrayColor];
    airFilterMileage.delegate = self;
    airFilterMileage.font = [UIFont textFont];
    [mileageOfLastView addSubview:airFilterMileage];
    
    tireMileage = [[UITextField alloc]initWithFrame:CGRectMake(0, airFilterMileage.frame.origin.y + airFilterMileage.frame.size.height, mileageOfLastView.frame.size.width, 40)];
    tireMileage.textColor = [UIColor lightGrayColor];
    tireMileage.delegate = self;
    tireMileage.font = [UIFont textFont];
    [mileageOfLastView addSubview:tireMileage];

    oilChangeFreq = [[UITextField alloc]initWithFrame:CGRectMake(5, 0, freqView.frame.size.width - 5, 40)];
    oilChangeFreq.textColor = [UIColor lightGrayColor];
    oilChangeFreq.text = @"4000";
    oilChangeFreq.delegate = self;
    oilChangeFreq.font = [UIFont textFont];
    [freqView addSubview:oilChangeFreq];
    
    tireRotationFreq = [[UITextField alloc]initWithFrame:CGRectMake(5, oilChangeFreq.frame.origin.y  + oilChangeFreq.frame.size.height, freqView.frame.size.width - 5, 40)];
    tireRotationFreq.textColor = [UIColor lightGrayColor];
    tireRotationFreq.delegate = self;
    tireRotationFreq.text = @"6000";
    tireRotationFreq.font = [UIFont textFont];
    [freqView addSubview:tireRotationFreq];
    
    brakePadsFreq = [[UITextField alloc]initWithFrame:CGRectMake(5, tireRotationFreq.frame.origin.y  + tireRotationFreq.frame.size.height, freqView.frame.size.width - 5, 40)];
    brakePadsFreq.textColor = [UIColor lightGrayColor];
    brakePadsFreq.delegate = self;
    brakePadsFreq.text = @"50000";
    brakePadsFreq.font = [UIFont textFont];
    [freqView addSubview:brakePadsFreq];
    
    accessoryDriveBeltFreq = [[UITextField alloc]initWithFrame:CGRectMake(5, brakePadsFreq.frame.origin.y  + brakePadsFreq.frame.size.height, freqView.frame.size.width -5, 40)];
    accessoryDriveBeltFreq.textColor = [UIColor lightGrayColor];
    accessoryDriveBeltFreq.delegate = accessoryDriveBeltMileage.delegate;
    accessoryDriveBeltFreq.text = @"50000";
    accessoryDriveBeltFreq.font = [UIFont textFont];
    [freqView addSubview:accessoryDriveBeltFreq];
    
    coolantHosesFreq = [[UITextField alloc]initWithFrame:CGRectMake(5, accessoryDriveBeltFreq.frame.origin.y  + accessoryDriveBeltFreq.frame.size.height, freqView.frame.size.width - 5, 40)];
    coolantHosesFreq.textColor = [UIColor lightGrayColor];
    coolantHosesFreq.delegate = coolantHosesMileage.delegate;
    coolantHosesFreq.text = @"75000";
    coolantHosesFreq.font = [UIFont textFont];
    [freqView addSubview:coolantHosesFreq];
    
    airFilterFreq = [[UITextField alloc]initWithFrame:CGRectMake(5, coolantHosesFreq.frame.origin.y  + coolantHosesFreq.frame.size.height, freqView.frame.size.width-5, 40)];
    airFilterFreq.textColor = [UIColor lightGrayColor];
    airFilterFreq.delegate = airFilterMileage.delegate;
    airFilterFreq.text = @"30000";
    airFilterFreq.font = [UIFont textFont];
    [freqView addSubview:airFilterFreq];
    
    tireFreq = [[UITextField alloc]initWithFrame:CGRectMake(5, airFilterFreq.frame.origin.y  + airFilterFreq.frame.size.height, freqView.frame.size.width -5, 40)];
    tireFreq.textColor = [UIColor lightGrayColor];
    tireFreq.delegate = tireMileage.delegate;
    tireFreq.text = @"45000";
    tireFreq.font = [UIFont textFont];
    [freqView addSubview:tireFreq];
    
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Done Editing" forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, self.view.frame.size.width, 50);
    btn.tintColor = [UIColor blackColor];
    [btn setBackgroundImage:[UIImage imageNamed:@"RedBtn"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"RedBtnTouched"] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(doneEditing:) forControlEvents:UIControlEventTouchUpInside];
    tireFreq.inputAccessoryView = btn;
    tireMileage.inputAccessoryView = btn;
    
    
    
}

-(void)saveBtnTouched:(UIButton*)saveBtn{
    if ([carName.text isEqualToString:@""] || [curMileage.text isEqualToString:@""] || [oilChangeMileage.text isEqualToString:@""] || [oilChangeFreq.text isEqualToString:@""] || [tireRotationMileage.text isEqualToString:@""] || [tireRotationFreq.text isEqualToString:@""] || [brakePadsMileage.text isEqualToString:@""] || [brakePadsFreq.text isEqualToString:@""] || [accessoryDriveBeltMileage.text isEqualToString:@""] || [accessoryDriveBeltFreq.text isEqualToString:@""] || [coolantHosesMileage.text isEqualToString:@""] || [coolantHosesFreq.text isEqualToString:@""] || [airFilterMileage.text isEqualToString:@""] || [airFilterFreq.text isEqualToString:@""] || [tireMileage.text isEqualToString:@""] || [tireFreq.text isEqualToString:@""]) {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You must complete all fields"delegate:nil cancelButtonTitle:@"Ok"  otherButtonTitles:nil];
        [alert show];
    } else {
        [self addCar];
    }
}

-(void)addCar{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterNoStyle;
    
    NSNumber* curMileageNum = [f numberFromString:curMileage.text];
    NSNumber* oilChangeMileageNum = [f numberFromString:oilChangeMileage.text];
    NSNumber* oilChangeFreqNum = [f numberFromString:oilChangeFreq.text];
    NSNumber* tireRotationMileageNum = [f numberFromString:tireRotationMileage.text];
    NSNumber* tireRotationFreqNum = [f numberFromString:tireRotationFreq.text];
    NSNumber* brakePadsMileageNum = [f numberFromString:brakePadsMileage.text];
    NSNumber* brakePadsFreqNum = [f numberFromString:brakePadsFreq.text];
    NSNumber* accessoryDriveBeltMileageNum = [f numberFromString:accessoryDriveBeltMileage.text];
    NSNumber* accessoryDriveBeltFreqNum = [f numberFromString:accessoryDriveBeltFreq.text];
    NSNumber* coolantHosesMileageNum = [f numberFromString:coolantHosesMileage.text];
    NSNumber* coolantHosesFreqNum = [f numberFromString:coolantHosesFreq.text];
    NSNumber* airFilterMileageNum = [f numberFromString:airFilterMileage.text];
    NSNumber* airFilterFreqNum = [f numberFromString:airFilterFreq.text];
    NSNumber* tireMileageNum = [f numberFromString:tireMileage.text];
    NSNumber* tireFreqNum = [f numberFromString:tireFreq.text];
    
    PFObject* car = [PFObject objectWithClassName:@"Car"];
    [car setObject:_user forKey:@"username"];
    [car setObject:carName.text forKey:@"carName"];
    [car setObject:curMileageNum forKey:@"curMileage"];
    
    PFObject* oilChangePart = [PFObject objectWithClassName:@"Part"];
    [oilChangePart setObject:car forKey:@"carName"];
    [oilChangePart setObject:@"Oil Change" forKey:@"partName"];
    [oilChangePart setObject:oilChangeMileageNum forKey:@"mileageOfLast"];
    [oilChangePart setObject:oilChangeFreqNum forKey:@"frequency"];
    
    PFObject* tireRotationPart = [PFObject objectWithClassName:@"Part"];
    [tireRotationPart setObject:car forKey:@"carName"];
    [tireRotationPart setObject:@"Tire Rotation" forKey:@"partName"];
    [tireRotationPart setObject:tireRotationMileageNum forKey:@"mileageOfLast"];
    [tireRotationPart setObject:tireRotationFreqNum forKey:@"frequency"];
    
    PFObject* brakePadsPart = [PFObject objectWithClassName:@"Part"];
    [brakePadsPart setObject:car forKey:@"carName"];
    [brakePadsPart setObject:@"Brake Pads" forKey:@"partName"];
    [brakePadsPart setObject:brakePadsMileageNum forKey:@"mileageOfLast"];
    [brakePadsPart setObject:brakePadsFreqNum forKey:@"frequency"];
    
    PFObject* accessoryDriveBeltPart = [PFObject objectWithClassName:@"Part"];
    [accessoryDriveBeltPart setObject:car forKey:@"carName"];
    [accessoryDriveBeltPart setObject:@"Accessory Drive Belt" forKey:@"partName"];
    [accessoryDriveBeltPart setObject:accessoryDriveBeltMileageNum forKey:@"mileageOfLast"];
    [accessoryDriveBeltPart setObject:accessoryDriveBeltFreqNum forKey:@"frequency"];
    
    PFObject* coolantHosesPart = [PFObject objectWithClassName:@"Part"];
    [coolantHosesPart setObject:car forKey:@"carName"];
    [coolantHosesPart setObject:@"Coolant Hoses" forKey:@"partName"];
    [coolantHosesPart setObject:coolantHosesMileageNum forKey:@"mileageOfLast"];
    [coolantHosesPart setObject:coolantHosesFreqNum forKey:@"frequency"];
    
    PFObject* airFilterPart = [PFObject objectWithClassName:@"Part"];
    [airFilterPart setObject:car forKey:@"carName"];
    [airFilterPart setObject:@"Air Filter" forKey:@"partName"];
    [airFilterPart setObject:airFilterMileageNum forKey:@"mileageOfLast"];
    [airFilterPart setObject:airFilterFreqNum forKey:@"frequency"];
    
    PFObject* tirePart = [PFObject objectWithClassName:@"Part"];
    [tirePart setObject:car forKey:@"carName"];
    [tirePart setObject:@"Tires" forKey:@"partName"];
    [tirePart setObject:tireMileageNum forKey:@"mileageOfLast"];
    [tirePart setObject:tireFreqNum forKey:@"frequency"];
    
    parts = @[oilChangePart, tireRotationPart, brakePadsPart, accessoryDriveBeltPart, coolantHosesPart, airFilterPart, tirePart];
    
    [car saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            //[self.navigationController popViewControllerAnimated:YES];
            [self saveParts];
        }
    }];
}

-(void)saveParts{
    [PFObject saveAllInBackground:parts block:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

-(void)doneEditing:(id)sender{
    [tireMileage resignFirstResponder];
    [tireFreq resignFirstResponder];
}

-(void)closeKeyboard{
    [carName resignFirstResponder];
    [curMileage resignFirstResponder];
    [oilChangeMileage resignFirstResponder];
    [oilChangeFreq resignFirstResponder];
    [tireRotationMileage resignFirstResponder];
    [tireRotationFreq resignFirstResponder];
    [brakePadsMileage resignFirstResponder];
    [brakePadsFreq resignFirstResponder];
    [accessoryDriveBeltMileage resignFirstResponder];
    [accessoryDriveBeltFreq resignFirstResponder];
    [airFilterMileage resignFirstResponder];
    [airFilterFreq resignFirstResponder];
    [tireMileage resignFirstResponder];
    [tireFreq resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)sender{
    [self closeKeyboard];
    return NO;
}

-(void)cancelKeyboard:(id)sender{
    [self closeKeyboard];
}

-(void)textFieldDidBeginEditing:(UITextField *)sender{
    if (sender == accessoryDriveBeltMileage || sender == accessoryDriveBeltFreq) {
        [UIView animateWithDuration:.5 animations:^{
            self.view.frame = CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height);
        }];
    } else if (sender == coolantHosesMileage || sender == coolantHosesFreq){
        [UIView animateWithDuration:.5 animations:^{
            self.view.frame = CGRectMake(0, -60, self.view.frame.size.width, self.view.frame.size.height);
        }];
    } else if (sender == airFilterMileage || sender == airFilterFreq){
        [UIView animateWithDuration:.5 animations:^{
            self.view.frame = CGRectMake(0, -100, self.view.frame.size.width, self.view.frame.size.height);
        }];
    } else if (sender == tireMileage || sender == tireFreq){
        [UIView animateWithDuration:.5 animations:^{
            self.view.frame = CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
        }];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)sender{
    
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
