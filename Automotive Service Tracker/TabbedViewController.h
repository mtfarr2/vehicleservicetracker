//
//  TabbedViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabbedViewController : UITabBarController

@end
