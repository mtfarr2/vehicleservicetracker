//
//  RootTabbedViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabbedViewController.h"
#import "UpdateMileageViewController.h"
#import "MaintenanceHistoryViewController.h"
#import "UpcomingMaintenanceViewController.h"

@interface RootTabbedViewController : UIViewController

@end
