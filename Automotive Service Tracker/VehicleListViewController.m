//
//  VehicleListViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/1/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "VehicleListViewController.h"

@interface VehicleListViewController ()

@end

@implementation VehicleListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"My Cars";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewCarTouched:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    
    
    myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height - 70) style:UITableViewStyleGrouped];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.backgroundColor = [UIColor clearColor];
    myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:myTableView];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    PFQuery* query = [PFQuery queryWithClassName:@"Car"];
    [query whereKey:@"username" equalTo:_user];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        usersCars = objects;
        if(!error){
            if (usersCars.count > ad.carObjects.count) {
                for (long i = ad.carObjects.count; i < usersCars.count; i++) {
                    PFObject* dictionary = [usersCars objectAtIndex:i];
                    
                    Car* car = [[Car new]initWithObject:dictionary];
                    
                    [ad.carObjects addObject:car];
                }
            }
            [self loadData];
        }
        
    }];
    
    
}

-(void)loadData{
    [myTableView reloadData];
}

-(void)addNewCarTouched:(id)sender{
    AddNewCarViewController* ancvc = [AddNewCarViewController new];
    ancvc.user = _user;
    [self.navigationController pushViewController:ancvc animated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return usersCars.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"tableViewCell"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"tableViewCellTouched"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    PFObject* dictionary = [usersCars objectAtIndex:indexPath.row];
    
    UILabel* title = [[UILabel alloc]initWithFrame:CGRectMake(5, 9, self.view.frame.size.width, 40)];
    NSString *cellCarName = dictionary[@"carName"];
    title.text = [NSString stringWithFormat:@" %@",cellCarName];
    title.textColor = [UIColor lightGrayColor];
    title.textAlignment = NSTextAlignmentCenter;
    cell.backgroundColor = [UIColor headerColor];
    [cell addSubview:title];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //todo
    RootTabbedViewController* rtvc = [[RootTabbedViewController alloc]init];
    ad.curCar = [ad.carObjects objectAtIndex:indexPath.row];
    ad.curPFCar = [usersCars objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:rtvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
