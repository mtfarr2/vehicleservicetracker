//
//  ForgotPasswordViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/21/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Password Recovery";
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    email = [[UITextField alloc]initWithFrame:CGRectMake(0, 200, self.view.frame.size.width, 50)];
    email.backgroundColor = [UIColor headerColor];
    UIColor *color = [UIColor lightTextColor];
    email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Enter your email address" attributes:@{NSForegroundColorAttributeName: color}];
    email.textColor = [UIColor lightGrayColor];
    [self.view addSubview:email];
    
    
    submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame = CGRectMake(self.view.frame.size.width/4, email.frame.origin.y + email.frame.size.height + 100, self.view.frame.size.width/2, 50);
    [submitBtn setBackgroundImage:[UIImage imageNamed:@"RedBtn"] forState:UIControlStateNormal];
    [submitBtn setBackgroundImage:[UIImage imageNamed:@"RedBtnTouched"] forState:UIControlStateHighlighted];
    [submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
}

-(void)submitBtnTouched:(id)sender{
    [PFUser requestPasswordResetForEmailInBackground:email.text];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
