//
//  Car.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/16/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "Car.h"

@implementation Car

-(instancetype)initWithObject:(PFObject *)dictionary{
    self = [super init];
    if (self) {
        self.carName = [dictionary objectForKey:@"carName"];
        self.curMileage = [dictionary objectForKey:@"curMileage"];
        self.carID = dictionary.objectId;
    }
    return self;
}


@end
