//
//  UpcomingMaintenanceViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "AddMaintenanceViewController.h"

@interface UpcomingMaintenanceViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate* ad;
    UITableView* myTableView;
    NSArray* carParts;
}

@end
