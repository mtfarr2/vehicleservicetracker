//
//  AddMaintenanceViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/22/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "AddMaintenanceViewController.h"

@interface AddMaintenanceViewController ()

@end

@implementation AddMaintenanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Add Maintenance";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addMaintenanceBackground"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    UITapGestureRecognizer* tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelKeyboard:)];
    [self.view addGestureRecognizer:tgr];
    
   
//    self.view.backgroundColor = [UIColor lightGrayColor];
    
    
    
    arrayOfTitles = @[@"Accessory Drive Belt", @"Air Filter", @"Brake Pads", @"Coolant Hoses", @"Oil Change", @"Tire Rotation", @"Tires", @"Custom"];
    partName = @"Accessory Drive Belt";
    picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 216)];
    picker.dataSource = self;
    picker.delegate = self;
    [self.view addSubview:picker];
    
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, picker.frame.origin.y + picker.frame.size.height + 50, self.view.frame.size.width, 216)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    dateString = [formatter stringFromDate:datePicker.date];
    [self.view addSubview:datePicker];
    
    UIButton* nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(self.view.frame.size.width / 4, datePicker.frame.origin.y + datePicker.frame.size.height + 20, self.view.frame.size.width / 2, 50);
    [nextBtn setBackgroundImage:[UIImage imageNamed:@"RedBtn"] forState:UIControlStateNormal];
    [nextBtn setBackgroundImage:[UIImage imageNamed:@"RedBtnTouched"] forState:UIControlStateHighlighted];
    [nextBtn addTarget:self action:@selector(nextBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn setTitle:@"Next" forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:nextBtn];
    

   
}

-(void)cancelKeyboard:(id)sender{
    [customName resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)sender{
   
    [customName resignFirstResponder];
    
    return NO;
}



-(void)nextBtnTouched:(id)sender
{
    AddMaintenancePageTwoViewController* amptvc = [AddMaintenancePageTwoViewController new];
    amptvc.carParts = _carParts;
    amptvc.maintDate = datePicker.date;
    if ([partName isEqualToString:@""]){
        amptvc.partName = customName.text;
    }else{
        amptvc.partName = partName;
    }
    [self.navigationController pushViewController:amptvc animated:YES];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arrayOfTitles.count;
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return arrayOfTitles[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([arrayOfTitles[row]  isEqual: @"Custom"]) {
        textFieldView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/8, picker.frame.origin.y + picker.frame.size.height, self.view.frame.size.width - self.view.frame.size.width / 4, 40)];
        textFieldView.backgroundColor = [UIColor headerColor];
        textFieldView.layer.cornerRadius = 5;
        [self.view addSubview:textFieldView];
        
        enterPartNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 130, 40)];
        enterPartNameLbl.text = @"Enter Part Name: ";
        enterPartNameLbl.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
        enterPartNameLbl.textColor = [UIColor lightGrayColor];
        [textFieldView addSubview:enterPartNameLbl];
        
        customName = [[UITextField alloc]initWithFrame:CGRectMake(enterPartNameLbl.frame.origin.x + enterPartNameLbl.frame.size.width, 0, textFieldView.frame.size.width - enterPartNameLbl.frame.size.width, 40)];
        customName.textColor = [UIColor lightGrayColor];
        customName.delegate = self;
        [textFieldView addSubview:customName];
        partName = @"";
    } else {
        [textFieldView removeFromSuperview];
        partName = arrayOfTitles[row];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
