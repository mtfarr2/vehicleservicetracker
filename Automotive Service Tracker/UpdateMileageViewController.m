//
//  UpdateMileageViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "UpdateMileageViewController.h"

@interface UpdateMileageViewController ()

@end

@implementation UpdateMileageViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self){
        self.title = @"Update Mileage";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Update Mileage";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    updateMileageField = [[UITextField alloc]initWithFrame:CGRectMake(0, 150, self.view.frame.size.width, 50)];
    updateMileageField.backgroundColor = [UIColor headerColor];
    UIColor *color = [UIColor lightTextColor];
    updateMileageField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Enter current mileage" attributes:@{NSForegroundColorAttributeName: color}];
    updateMileageField.textColor = [UIColor lightGrayColor];
    [self.view addSubview:updateMileageField];
    
    submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame = CGRectMake(self.view.frame.size.width/4, updateMileageField.frame.origin.y + updateMileageField.frame.size.height + 100, self.view.frame.size.width/2, 50);
    [submitBtn setBackgroundImage:[UIImage imageNamed:@"RedBtn"] forState:UIControlStateNormal];
    [submitBtn setBackgroundImage:[UIImage imageNamed:@"RedBtnTouched"] forState:UIControlStateHighlighted];
    [submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UINavigationController *cont = (UINavigationController*) self.parentViewController.parentViewController;
    
    cont.navigationItem.rightBarButtonItems = nil;
}

-(void)submitBtnTouched:(UIButton*)submitBtn{
    PFQuery *query = [PFQuery queryWithClassName:@"Car"];
    [query whereKey:@"username" equalTo:[PFUser currentUser]];
    [query whereKey:@"carName" equalTo: ad.curCar.carName];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject * car, NSError *error) {
        if (!error) {
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterNoStyle;
            
            NSNumber* curMileageNum = [f numberFromString:updateMileageField.text];
            if(curMileageNum > [car objectForKey:@"curMileage"]){
            
                [car setObject:curMileageNum forKey:@"curMileage"];
                
                [car saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if(!error){
                        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Success!" message:@"Successfully updated mileage"delegate:nil cancelButtonTitle:@"Ok"  otherButtonTitles:nil];
                        ad.curCar.curMileage = curMileageNum;
                        [alert show];
                    }
                }];
            } else {
                UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"New mileage must be greater than existing mileage"delegate:nil cancelButtonTitle:@"Ok"  otherButtonTitles:nil];
                [alert show];
            }
        } else {
            NSLog(@"Error: %@", error);
        }
    }];
    [updateMileageField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
