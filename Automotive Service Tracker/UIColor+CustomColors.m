//
//  UIColor+CustomColors.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 3/30/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+(UIColor*)backgroundColor{
    return [UIColor colorWithRed:37.0f/255.0f green:156.0f/255.0f blue:177.0f/255.0f alpha:1.0f];
}

+(UIColor*)headerColor{
    return [UIColor colorWithRed:46.0f/255.0f green:46.0f/255.0f blue:46.0f/255.0f alpha:1.0f];
}

@end
