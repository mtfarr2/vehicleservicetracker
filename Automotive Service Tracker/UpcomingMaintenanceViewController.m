//
//  UpcomingMaintenanceViewController.m
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/10/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "UpcomingMaintenanceViewController.h"

@interface UpcomingMaintenanceViewController ()

@end

@implementation UpcomingMaintenanceViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self){
        self.title = @"Upcoming Maintenance";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    self.tabBarController.tabBar.barTintColor =  [UIColor headerColor];

    self.navigationItem.title = @"Upcoming Maintenance";
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    self.view.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel* partNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(25, 110, 180, 40)];
    partNameLbl.text = @"Part Name";
    partNameLbl.textColor = [UIColor lightGrayColor];
    partNameLbl.textAlignment = NSTextAlignmentCenter;
    partNameLbl.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
    [self.view addSubview:partNameLbl];
    
    UILabel* milesToNextLbl = [[UILabel alloc]initWithFrame:CGRectMake(partNameLbl.frame.origin.x + partNameLbl.frame.size.width + 25, partNameLbl.frame.origin.y, 95, 40)];
    milesToNextLbl.text = @"Miles To Next";
    milesToNextLbl.textColor = [UIColor lightGrayColor];
    milesToNextLbl.textAlignment = NSTextAlignmentCenter;
    milesToNextLbl.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
    [self.view addSubview:milesToNextLbl];
    
    UIView* tableViewBackground = [[UIView alloc]initWithFrame:CGRectMake(25, 150, self.view.frame.size.width - 50, 307)];
    tableViewBackground.backgroundColor = [UIColor headerColor];
    tableViewBackground.layer.cornerRadius = 5;
    [self.view addSubview:tableViewBackground];
    
    myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, tableViewBackground.frame.size.width, tableViewBackground.frame.size.height) style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.backgroundColor = [UIColor clearColor];
    myTableView.scrollEnabled = NO;
    [tableViewBackground addSubview:myTableView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UINavigationController *cont = (UINavigationController*) self.parentViewController.parentViewController;
    
    cont.navigationItem.rightBarButtonItems = nil;
    
    
//    PFQuery* userQuery = [PFQuery queryWithClassName:@"User"];
//    userQuery whereKey:@"username" equalTo:
    PFQuery* query1 = [PFQuery queryWithClassName:@"Car"];
    [query1 whereKey:@"objectId" equalTo:ad.curCar.carID];
    PFQuery* query2 = [PFQuery queryWithClassName:@"Part"];
    [query2 whereKey:@"carName" matchesKey:@"objectId" inQuery:query1];
    [query2 orderByAscending:@"partName"];
    [query2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        carParts = objects;
        ad.carParts = objects;
        [self loadData];
        
    }];
}



-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor = [UIColor clearColor];
    
    //cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"tableViewCell"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    //cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"tableViewCellTouched"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    PFObject* dictionary = [carParts objectAtIndex:indexPath.row];
    
    UILabel* title = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 200, 40)];
    NSString *cellPartName = dictionary[@"partName"];
    title.text = [NSString stringWithFormat:@" %@",cellPartName];
    title.textColor = [UIColor lightGrayColor];
    title.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
    [cell addSubview:title];
    
    NSNumber* mileageOfLast = dictionary[@"mileageOfLast"];
    NSNumber* curMileage = ad.curCar.curMileage;
    NSNumber *mileSinceLast = [NSNumber numberWithInt:([curMileage intValue] - [mileageOfLast intValue])];
    NSNumber* mileageToNext = [NSNumber numberWithInt:([dictionary[@"frequency"] intValue] - [mileSinceLast intValue])];
    
    UILabel* milesToNext = [[UILabel alloc]initWithFrame:CGRectMake(title.frame.origin.x + title.frame.size.width + 25, 5, 75, 40)];
    milesToNext.font = [UIFont fontWithName:@"HoeflerText-Black" size:14];
    milesToNext.textAlignment = NSTextAlignmentRight;
    milesToNext.text = [NSString stringWithFormat:@"%@", mileageToNext];
    if ([mileageToNext intValue] > 1000) {
        milesToNext.textColor = [UIColor greenColor];
    } else if ([mileageToNext intValue] > 0 && [mileageToNext intValue] <= 1000){
        milesToNext.textColor = [UIColor yellowColor];
    } else {
        milesToNext.textColor = [UIColor redColor];
    }
    [cell addSubview:milesToNext];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddMaintenanceViewController* amvc = [AddMaintenanceViewController new];
    amvc.carParts = carParts;
    [self.navigationController pushViewController:amvc animated:YES];
}

-(void)loadData{
    [myTableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return carParts.count;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
