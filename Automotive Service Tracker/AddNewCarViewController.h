//
//  AddNewCarViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/7/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"


@interface AddNewCarViewController : UIViewController<UITextFieldDelegate>
{
    UILabel* carNameLbl;
    UITextField* carName;
    UILabel* curMileageLbl;
    UITextField* curMileage;
    UILabel* mileageOfLastLbl;
    UILabel* freqLbl;
    UILabel* oilChangeLbl;
    UITextField* oilChangeMileage;
    UITextField* oilChangeFreq;
    UILabel* tireRotationLbl;
    UITextField* tireRotationMileage;
    UITextField* tireRotationFreq;
    UILabel* brakePadsLbl;
    UITextField* brakePadsMileage;
    UITextField* brakePadsFreq;
    UILabel* accessoryDriveBeltLbl;
    UITextField* accessoryDriveBeltMileage;
    UITextField* accessoryDriveBeltFreq;
    UILabel* coolantHosesLbl;
    UITextField* coolantHosesMileage;
    UITextField* coolantHosesFreq;
    UILabel* airFilterLbl;
    UITextField* airFilterMileage;
    UITextField* airFilterFreq;
    UILabel* tireLbl;
    UITextField* tireMileage;
    UITextField* tireFreq;
    NSArray* parts;
    
}
@property (strong, nonatomic)PFUser* user;
@end
