//
//  SignUpViewController.h
//  Automotive Service Tracker
//
//  Created by Mark Farrell on 4/1/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import <Parse/Parse.h>
#import "VehicleListViewController.h"

@interface SignUpViewController : UIViewController
{
    UITextField* userName;
    UITextField* password;
    UITextField* confirmPassword;
    UITextField* email;
    UITextField* confirmEmail;
}

@end
